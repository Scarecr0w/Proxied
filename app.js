var express 		= require('express'),
	app 			= express();
var port 			= process.env.PORT || 1337;

app.use('/proxy', require('iproxy'))

app.listen(port);

console.log("Feeder listening @ " + port);